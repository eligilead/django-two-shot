from django.contrib.auth import views as auth_views
from django.urls import path
from accounts.views import signup


urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name="login"),
    # this link is connected to line-26 expenses.url it used to login users
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    # this link is connected to line-26 expenses.url it used to login users
    path("signup/", signup, name="signup"),
]
