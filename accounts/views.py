from django.shortcuts import render, redirect

# request comes form the browser,
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView
from django.contrib.auth.models import User
from django.contrib.auth import login

from receipts.models import Account

# Create your views here.


def signup(request):
    if request.method == "POST":
        # if sign is invoked it starts this method "POST"
        signup_request = UserCreationForm(request.POST)
        # This is django magic, UserCreationForm provides everything below
        if signup_request.is_valid():
            # if true
            username = request.POST.get("username")
            password = request.POST.get(
                "password1"
            )  # in parameters has to be (password1)
            # storing username and password, create_user line-26 add info to database
            # lines 18,19,22 optional
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            # saving info that was put into the database
            login(request, user)
            # this allows the user to sign in after creation
            return redirect("home")
            # redirects to home page ^
    else:
        signup_request = UserCreationForm()
    context = {"form": signup_request}  #
    return render(request, "registration/signup.html", context)


"""   FUNCTION WITHOUT THE NOTES!
FUNCTION WITHOUT THE NOTES!
def signup(request):
    if request.method == "POST":
        signup_request = UserCreationForm(request.POST)
        if signup_request.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        signup_request = UserCreationForm()
    context = {"form": signup_request}
    return render(request, "registration/signups.html", context)
"""
