# from django.shortcuts import render
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect


# CreateView, DeleteView, UpdateView

from receipts.models import Receipt, ExpenseCategory, Account


# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipt_list"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
        # ^ with the foreign key you used this class makes
        # sure users are signed in to view stuff

    def __str__(self):
        return str(self.name)
        # this is used so you can see.


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    # using this show the exact instances you want to see ^, purchasers not needed, automatic
    context_object_name = "create_list"

    def form_valid(self, form):
        item = form.save(commit=False)
        # grabing all the info from the form, stored in item
        item.purchaser = self.request.user
        # setting it equal to whoever is currently signed in, saying who the user is.
        # purchaser is the user instance that made the request from http:request
        item.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categories/list.html"
    context_object_name = "expense_category_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)
        # ^ with the foreign key you used this class makes
        # sure users are signed in to view stuff

    def __str__(self):
        return str(self.name)
        # this is used so you can see.


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accounts/list.html"
    context_object_name = "account_list"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
        # ^ with the foreign key you used this class makes
        # sure users are signed in to view stuff

    def __str__(self):
        return str(self.name)
        # this is used so you can see.


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/categories/create.html"
    fields = ["name"]
    context_object_name = "expense_category_list"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/accounts/create.html"
    fields = ["name", "number"]
    context_object_name = "account_creation"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_view_list")
